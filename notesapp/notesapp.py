import os
global allmodsin
allmodsin = False
critmods = ["time","random","shutil","threading","keyboard","dearpygui","datetime","cryptography"]
while allmodsin == False:
    try:
        import time
        from datetime import datetime
        import random
        import shutil
        import socket
        import sys
        if getattr(sys, 'frozen', False):
            import pyi_splash # adds a loading screen so user doesnt x out because "It's not working" use pyinstaller --windowed --splash "notesapp_loadingscreen.jpg" --icon=notesapp_icon.ico --noconfirm notesapp.py
        import dearpygui.dearpygui as dpg
        import mouse
        import keyboard
        import threading
        import webbrowser
        import colorama
        import pickle
        from colorama import Fore, Back, Style
        import tqdm
        from cryptography.fernet import Fernet
        allmodsin = True
    except ModuleNotFoundError as e:
        e = str(e)
        sub = "'"
        errorstr = e.replace(sub,"*")
        errorsplit=errorstr.split("*")
        missmodule=errorsplit[1]
        if missmodule != "colorama":
            installmodule = input(f"error: you are missing module '{Fore.RED}{missmodule}{Fore.RESET}'. Would you like to try to install it? (Y/N) \n> ")
        else:
            installmodule = input(f"error: you are missing module '{missmodule}'. Would you like to try to install it? (Y/N) \n> ")
        if installmodule.lower() == "y" or installmodule.lower() == "yes":
            print("please wait. this may take a moment...")
            try:
                time.sleep(1)
            except:
                pass
            os.system(f"pip install {missmodule}")
            time.sleep(1)
            print("Done!")
            time.sleep(1)
        else:
            wishtotry = input(f"not installing this module most likely will cause the program to not function properly. Run anyway? (Y/N) \n> ")
            if wishtotry.lower() == "y" or wishtotry.lower() == "yes":
                print("loading...")
                if missmodule in critmods:
                    print("Error: critical module is missing! please install it!")
                    if input(f"do you want to go back to the start and try again? (you will have to install the module it wants you to install before you can use the program.) (Y/N)\n> ").lower() == "y":
                        print("ok! restarting!")
                    else:
                        exit()
                else:
                    print("loading...")
                    time.sleep(1)
                    os.system("cls")
                    print("while the missing module is not on the list of modules that are critical to the overall program function, certain features may not work as intended or just not work at all.\nUnexpected crashes due to this are not the fault of the programmer, but the fault of the user.")
            else:
                endorgoback = input(f"would you like to exit the program or restart & try again? type 'exit' or 'restart' \n> ")
                if endorgoback.lower() == "exit":
                    exit()
                else:
                    pass

dpg.create_context()
timenow = time.ctime()

SCRNWIDTH = 1280
SCRNHEIGHT = 720
global attempts
attempts = 3
global loggedin
loggedin = False
done1 = 0
done2 = 0
res1 = 0
res2 = 0
global hasreadinstructions
hasreadinstructions = False

# time

def gettime():
    global timenow
    global displaytime
    while True:
        timenow = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        time.sleep(0.1)
        dpg.set_value("time", f"{timenow}")

#login

filename = "notesapp_data/data/users.txt"
global dirpaths
dirpaths = []
global readmetext
readmetext = """READ:
- while the text files this saves to is encrypted, by no means do I consider this really secure, 
as it was written by a 14 year old in his free time, and the key file can be found pretty easily.

- the users file is even less secure, I added a lot of extra random numbers to hide user info, but it is by no means
very safe or secure, so I DO NOT recommend an extremely secure password.

HOW TO USE:

Sign in:

- run program, you can sign in by putting your username and password in their boxes. 
press enter to sign in, or press log in button. if you dont have an account, press sign up to make one.

Write notes:

- the take notes will make a text file with the name you put in the box (do not put .txt at the end it does it automatically) and write
the text you want to put there in the box. above there will be text saying what you wrote into the file so you can see what it now says.

- if the file already exists, press the edit button below the text box. then you can edit it, press ctrl+s to save it.

Read notes:

- the read notes button will show you the files that exist, again put the filename, (again, dont end it with .txt it does it automatically).
it will write what is written in the file in a dialog so you can read it.

Transfering notes:
- every user has a key that is set up when you log in. therefore, if you transfer to different file location or computer, you will not be able to interpret the old file.
- to fix this, go into notesapp_data/info_for_users/keys and find the one that is your-username_key.txt copy that over to the new file location/computer into the same location.
that way you can continue to edit files that were at the old location.
"""

def login():
    global loggedin
    global attempts
    global dirpaths
    global authorized_users
    global devs
    global usernames
    global readmetext
    
    devs = ["agg"]
    try:
        infile = open(filename,'rb')
    except:
        try:
            authuser = {}
            pos = random.randint(0,10000)
            authuser["guest123"] = "guest123"

            for l in range(10000):
                i = random.randint(-100000,100000)
                m = random.randint(-100000,100000)

                dpg.set_value(status, f"working on the users file now! your almost there. {l}/10000")
                authuser[m] = i
            
            with open(filename,'xb') as outfile:
                pickle.dump(authuser, outfile)

            infile = open(filename,'rb')
        except:
            status = dpg.add_text("logging in is taking longer because the file directory has not been created yet", indent=350, parent="loginwin", before=logbutton)
            os.makedirs("notesapp_data/data")

            time.sleep(1)

            authuser = {}
            pos = random.randint(0,10000)
            authuser["guest123"] = "guest123"

            for l in range(10000):
                i = random.randint(-100000,100000)
                m = random.randint(-100000,100000)

                dpg.set_value(status, f"working on the users file now! your almost there. {l}/10000")
                authuser[m] = i
            
            with open(filename,'xb') as outfile:
                pickle.dump(authuser, outfile)

            infile = open(filename,'rb')
            dpg.set_value(status, "                                     DONE!                                     ")

            # readmefile

    try:
        os.makedirs("notesapp_data/info_for_users/keys")
        readme = "notesapp_data/info_for_users/README.txt"
        try:
            userinfofile = open(readme,"xt")
        except:
            userinfofile = open(readme,"wt")
        userinfofile.write(readmetext)
        userinfofile.close()
        time.sleep(1)
    except:
        pass
    # get users
    authorized_users = pickle.load(infile)
    infile.close()

    usernames = []
    for usernameinfile in authorized_users:
        if isinstance(usernameinfile, str):
            usernames.append(usernameinfile)
    
    global user
    user = str(dpg.get_value(username))

    if user in authorized_users:

        userpassword = dpg.get_value(userpass)

        correctpass = authorized_users.get(user, "error 404. user key was verified to exist but error occured in retrieving the correct password.")

        if userpassword == correctpass:

            dpg.set_item_label(logbutton, "CORRECT!")
            dpg.show_item("syswin")
            dpg.show_item("timewin")
            dpg.show_item("notesmenu")
            dpg.show_item("showexport")
            dpg.delete_item("loginwindow")
            
            keyboard.remove_hotkey("enter")
            starttimefunc()
            loggedin = True
            directory = user
            parpath = f"{os.getcwd()}/notesapp_data/data/users/userdirectories"
            path = os.path.join(parpath, directory)
            if os.path.isdir(path):
                pass
            else:
                os.makedirs(path)
                try:
                    key = Fernet.generate_key()
                    with open(f"notesapp_data/info_for_users/keys/{user}_key.txt","xb") as outfile:
                        outfile.write(key)
                except:
                    pass
            dirall = os.listdir(path)
            for file in dirall:
                if os.path.isfile(os.path.join(os.curdir, file)):
                    dirpaths.append(file)
            dpg.set_value(filelist,f"existing files: {dirpaths} (do not add the .txt at the end of the filename. it adds it automatically and will not work if you do.)")
        else:
            attempts -= 1
            dpg.set_value(attemptstext, f"you have: {attempts} attempts left!")
            dpg.set_value(incorrectpass, value="          INCORRECT!          ")
            if attempts == 2:
                dpg.add_text("if you do not have an account, try making one below", indent=450, parent="loginwin", before=logbutton)
            if attempts <= 0:
                os.system('mshta javascript:alert("you ran out of attempts!");close()')
                exitfunc()
    else:
        attempts -= 1
        dpg.set_value(attemptstext, f"you have: {attempts} attempts left!")
        dpg.set_value(incorrectpass, value="          INCORRECT!          ")
        if attempts == 2:
            dpg.add_text("if you do not have an account, try making one below", indent=450, parent="loginwin", before=logbutton)
        if attempts == 1:
            dpg.add_text("you are going to be locked out if you do not make an account!", indent=425, parent="loginwin", before=logbutton)
        if attempts <= 0:
            exitfunc()
    # user data and callback set any time after button has been created

def signup():
    global hasreadinstructions
    try:
        infile = open(filename,'rb')
    except:
        try:
            authuser = {}
            pos = random.randint(0,10000)
            authuser["guest123"] = "guest123"
            for l in range(10000):
                i = random.randint(-100000,100000)
                m = random.randint(-100000,100000)
                dpg.set_value(status, f"working on the users file now! your almost there. {l}/10000")
                authuser[m] = i
            outfile = open(filename,'xb')
            pickle.dump(authuser, outfile)
            outfile.close()
            infile = open(filename,'rb')
        except:
            status = dpg.add_text("logging in is taking longer because the file directory has not been created yet", indent=350, parent="loginwin", before=logbutton)
            os.makedirs("notesapp_data/data")
            time.sleep(1)

            authuser = {}
            pos = random.randint(0,10000)
            authuser["guest123"] = "guest123"
            for l in range(10000):
                i = random.randint(-100000,100000)
                m = random.randint(-100000,100000)

                dpg.set_value(status, f"        working on the users file now! your almost there. {l}/10000")
                authuser[m] = i
            outfile = open(filename,'xb')
            pickle.dump(authuser, outfile)
            outfile.close()

            infile = open(filename,'rb')
            dpg.set_value(status, "                                     DONE!                                     ")

            # readmefile
    try:
        os.makedirs("notesapp_data/info_for_users/keys")
        readme = "notesapp_data/info_for_users/README.txt"
        try:
            userinfofile = open(readme,"xt")
        except:
            userinfofile = open(readme,"wt")
        userinfofile.write(readmetext)
        userinfofile.close()
        time.sleep(1)
    except:
        pass
    
    authusers = pickle.load(infile)
    infile.close()
    global user
    user = str(dpg.get_value(username))
    userexists = dpg.add_text(" ", indent=475, parent="loginwin", before=logbutton)
    authorized_users = {}
    for key in authusers:
        if isinstance(key,str):
            authorized_users[key] = authusers.get(key,"error")

    if hasreadinstructions == False:
        dpg.set_value(userexists, "Sorry, but you must click the info button!")

    if user in authorized_users:
        dpg.set_value(userexists, "   Sorry, but that user already exists!   ")
        
    elif (user not in authorized_users) and hasreadinstructions == True:
        newpass = dpg.get_value(userpass)
        authusers = {}
        authorized_users[user] = newpass
        for key in authorized_users:
            pos = random.randint(0,1000)
            for l in range(1000):
                i = random.randint(-1000000,1000000)
                m = random.randint(-1000000,1000000)
                if l == pos:
                    authusers[key] = authorized_users.get(key, "error")
                authusers[i] = m
        outfile = open(filename, "wb")
        pickle.dump(authusers,outfile)
        outfile.close()
        dpg.add_text("Account creation succeeded! you will be in shortly!", indent=450, parent="loginwin", before=logbutton)
        time.sleep(2)
        dpg.delete_item(userexists)
        dpg.delete_item(sign_up)
        login()

    # user data and callback set any time after button has been created

def exitfunc():
    try:
        sys.exit()
    except Exception as e:
        print("bye!")

# HELP

def close_help():
    dpg.hide_item("instructions")
    keyboard.remove_hotkey("esc")

def instructionsfunc():
    global readmetext
    global hasreadinstructions
    hasreadinstructions = True
    dpg.set_value(instructions,f"{readmetext}")
    keyboard.add_hotkey("esc",close_help)
    dpg.show_item("instructions")

# login

with dpg.window(label="login", pos=[0,0], width=SCRNWIDTH, height=SCRNHEIGHT,no_background=False, menubar=False, no_move=True, no_resize=True, no_title_bar=True, tag="loginwindow"):
    # user data and callback set when button is created
    attemptstext = dpg.add_text(default_value=f"you have: {attempts} attempts left!", indent=(SCRNWIDTH/2-110))
    usernamelabel = dpg.add_text(default_value="         username:        ", indent=(SCRNWIDTH/2-110))
    username = dpg.add_input_text(label=" ", tag="user", default_value="", width=200, indent=(SCRNWIDTH/2-110))
    passwordlabel = dpg.add_text(default_value="         password:        ", indent=(SCRNWIDTH/2-105))
    userpass = dpg.add_input_text(label=" ", tag="pass", default_value="", width=200, indent=(SCRNWIDTH/2-110), password=True)
    incorrectpass = dpg.add_text(default_value="                              ", color=(255,0,0), indent=(SCRNWIDTH/2-110))
    logbutton = dpg.add_button(label="           login           ", callback=login, indent=(SCRNWIDTH/2-110))
    sign_up = dpg.add_button(label="          sign up          ", callback=signup, indent=(SCRNWIDTH/2-110))
    byebutton = dpg.add_button(label="           exit            ", callback=exitfunc, indent=(SCRNWIDTH/2-110))
    loginhelpbutton = dpg.add_button(label="           info            ", callback=instructionsfunc,indent=(SCRNWIDTH/2-110))
    friendlyadvice = dpg.add_text(default_value="If you dont want to create an account, you can use guest123 for user and password. however, your files will not be secure!",indent=250)

# encryption

def encrypt(words,user):
    with open(f"notesapp_data/info_for_users/keys/{user}_key.txt", "rb") as infile:
        key = infile.read()
    fernet = Fernet(key)
    encMessage = fernet.encrypt(words.encode())
    return encMessage

def decrypt(words,user):
    with open(f"notesapp_data/info_for_users/keys/{user}_key.txt", "rb") as infile:
        key = infile.read()
    fernet = Fernet(key)
    decMessage = fernet.decrypt(words).decode()
    return decMessage

# EXPORTS AND IMPORTS WINDOWS AND FUNCTIONS

def export():
    SEPARATOR = "<SEPARATOR>"
    BUFFER_SIZE = 4096 # send 4096 bytes each time step
    host = dpg.get_value(serverip)
    # the port, let's use 5001
    port = 5001
    # the name of file we want to send, make sure it exists
    
    filename = f"notesapp_data/data/users/userdirectories/{user}/{dpg.get_value(sendfile)}"
    try:
        outfile = open(filename,"x")
        outfile.write(encrypt("ERROR: THE SENDER PUT IN AN INVALID FILENAME!",user))
        outfile.close()
    except FileExistsError:
        pass
    # get the file size
    filesize = os.path.getsize(filename)

    s = socket.socket()
    dpg.set_value(sendstatus,f"{dpg.get_value(sendstatus)}\n[+] Connecting to {host}:{port}")
    s.connect((host, port))
    dpg.set_value(sendstatus,f"{dpg.get_value(sendstatus)}\n[+] Connected! sending...")
    s.send(f"{user}{SEPARATOR}{filename}{SEPARATOR}{filesize}".encode())
    progress = tqdm.tqdm(range(filesize), f"Sending {filename}", unit="B", unit_scale=True, unit_divisor=1024)
    with open(filename, "rb") as f:
        while True:
            # read the bytes from the file
            bytes_read = f.read(BUFFER_SIZE)
            if not bytes_read:
                # file transmitting is done
                break
            # we use sendall to assure transimission in 
            # busy networks
            s.sendall(bytes_read)
            # update the progress bar
            progress.update(len(bytes_read))
    # close the socket
    dpg.set_value(sendstatus,f"{dpg.get_value(sendstatus)}\n[+] file transfer complete! closing socket")
    s.close()
    hideexport()

with dpg.window(label="export menu", pos=[140,100], width=1000,height=500,no_move=False,no_resize=True,menubar=False, show=False, tag="export", no_collapse=True):
    # user data and callback set when button is created
    sendstatus = dpg.add_text(default_value="",wrap=0,color=(0,255,0),indent=10)
    serverboxlabel = dpg.add_text("SERVER IP:",pos=[10,300])
    serverip = dpg.add_input_text(default_value="",pos=[10,350])
    sendfilelabel = dpg.add_text("file (MUST BE IN YOUR DIRECTORY! MUST END WITH .<fileextension> most likely: .txt):",pos=[10,400])
    sendfile = dpg.add_input_text(default_value="",pos=[10,450])
    sendbutton = dpg.add_button(label="send",callback=export,pos=[10,500])

def showexport():
    if dpg.is_item_shown("import"):
        hideimport()
    dpg.show_item("export")
    
    try:
        keyboard.remove_hotkey("enter")
    except:
        pass
    try:
        keyboard.remove_hotkey("esc")
    except:
        pass
    keyboard.add_hotkey("esc",hideexport)

def hideexport():
    dpg.hide_item("export")
    keyboard.remove_hotkey("esc")
    try:
        keyboard.remove_all_hotkeys()
        keyboard.add_hotkey("f11", fullscreen)
        keyboard.add_hotkey("esc + x", exitfunc)
    except:
        pass

# IMPORT FILES:

def importfunc():
    # device's IP address
    SERVER_HOST = "0.0.0.0"
    # server ip 0.0.0.0 so that it reachable at all of its ips
    SERVER_PORT = 5001
    # receive 4096 bytes each time
    BUFFER_SIZE = 4096
    SEPARATOR = "<SEPARATOR>"

    # create the server socket
    # TCP socket
    s = socket.socket()
    # bind the socket to our local address
    s.bind((SERVER_HOST, SERVER_PORT))

    # enabling our server to accept connections
    # 5 here is the number of unaccepted connections that
    # the system will allow before refusing new connections
    
    s.listen(5)
    dpg.set_value(importstatus,f"[*] Listening as {SERVER_HOST}:{SERVER_PORT}")

    # accept connection if there is any
    client_socket, address = s.accept() 
    # if below code is executed, that means the sender is connected
    dpg.set_value(importstatus,f"{dpg.get_value(importstatus)}\n[+] {address} is connected.")

    # receive the file infos
    # receive using client socket, not server socket
    received = client_socket.recv(BUFFER_SIZE).decode()
    usersent,filename,filesize = received.split(SEPARATOR)
    print(received.split(SEPARATOR))
    # remove absolute path if there is
    filename = os.path.basename(filename)
    # convert to integer
    filesize = int(filesize)
    # start receiving the file from the socket
    # and writing to the file stream
    progress = tqdm.tqdm(range(filesize), f"Receiving {filename} from {usersent}", unit="B", unit_scale=True, unit_divisor=1024)
    with open(f"notesapp_data/data/users/userdirectories/{usersent}/{filename}", "wb") as f:
        while True:
            # read 1024 bytes from the socket (receive)
            bytes_read = client_socket.recv(BUFFER_SIZE)
            if not bytes_read:    
                # nothing is received
                # file transmitting is done
                break
            # write to the file the bytes we just received
            f.write(bytes_read)
            # update the progress bar
            progress.update(len(bytes_read))
    if usersent != user:
        shutil.copy(f"notesapp_data/data/users/userdirectories/{usersent}/{filename}", f"notesapp_data/data/users/userdirectories/{user}/{filename}")
    # close the client socket
    client_socket.close()
    # close the server socket
    s.close()
    dpg.set_value(importstatus,f"{dpg.get_value(importstatus)}\n[+] file transfer complete. closing socket!")
    hideimport()

with dpg.window(label="import menu", pos=[140,50], width=1000,height=600,no_move=False,no_resize=True,menubar=False, show=False, tag="import", no_collapse=True):
    # user data and callback set when button is created
    importstatus = dpg.add_text(default_value="",wrap=0,color=(0,255,0),indent=10)
    importbutton = dpg.add_button(label="import",callback=importfunc,pos=[10,500])

def showimport():
    if dpg.is_item_shown("export"):
        hideexport()
    dpg.show_item("import")
    
    try:
        keyboard.remove_hotkey("enter")
    except:
        pass
    try:
        keyboard.remove_hotkey("esc")
    except:
        pass
    keyboard.add_hotkey("esc",hideimport)

def hideimport():
    dpg.hide_item("import")
    keyboard.remove_hotkey("esc")
    try:
        keyboard.remove_all_hotkeys()
        keyboard.add_hotkey("f11", fullscreen)
        keyboard.add_hotkey("esc + x", exitfunc)
    except:
        pass

# NOTES APP SHOW OR HIDE FUNCTIONS

def hidewritenotes():
    dpg.hide_item("takenotes")
    dpg.hide_item("displaynotes")
    dpg.hide_item(wtext)
    dpg.hide_item(labelwrite2)
    dpg.hide_item(savefile)
    dpg.set_value(wtext,"")
    dpg.show_item("notesmenu")
    dpg.show_item("showexport")
    dpg.set_value(notestext,"")
    keyboard.remove_hotkey("esc")
    keyboard.remove_hotkey("ctrl+s")

def hidereadnotes():
    global devs
    if user in devs:
        dpg.hide_item("readnotesdev")
        dpg.hide_item("displaynotes")
        dpg.show_item("notesmenu")
        dpg.show_item("showexport")
        dpg.set_value(notestext,"")
        keyboard.remove_hotkey("esc")
        dpg.set_value(rfiledev,"")
        dpg.set_value(ruser,"")
    else:
        dpg.hide_item("readnotes")
        dpg.hide_item("displaynotes")
        dpg.show_item("notesmenu")
        dpg.show_item("showexport")
        dpg.set_value(notestext,"")
        keyboard.remove_hotkey("esc")
        dpg.set_value(rfile,"")

def showwritenotes():
    dpg.show_item("takenotes")
    dpg.show_item("displaynotes")
    dpg.hide_item("notesmenu")
    dpg.hide_item("showexport")
    try:
        keyboard.remove_hotkey("enter")
    except:
        pass
    keyboard.add_hotkey("enter",writefile)
    try:
        keyboard.remove_hotkey("esc")
    except:
        pass
    keyboard.add_hotkey("esc",hidewritenotes)

def showreadnotes():
    if user in devs:
        dpg.show_item("readnotesdev")
        dpg.set_value(labeluser, f"WHICH USER WOULD YOU LIKE TO ACCESS THE FILES OF? users: {usernames}")
        
        dpg.show_item("displaynotes")
        dpg.hide_item("notesmenu")
        dpg.hide_item("showexport")
        try:
            keyboard.remove_hotkey("enter")
        except:
            pass
        keyboard.add_hotkey("enter",accessanddisplayfiles)
        try:
            keyboard.remove_hotkey("esc")
        except:
            pass
        keyboard.add_hotkey("esc",hidereadnotes)
    else:
        dpg.show_item("readnotes")
        dpg.show_item("displaynotes")
        dpg.hide_item("notesmenu")
        dpg.hide_item("showexport")
        accessanddisplayfilesnondevedition()
        try:
            keyboard.remove_hotkey("enter")
        except:
            pass
        keyboard.add_hotkey("enter",readnotes)
        try:
            keyboard.remove_hotkey("esc")
        except:
            pass
        keyboard.add_hotkey("esc",hidereadnotes)

# NOTES APP WRITE FUNCTIONS

def writefile():
    global dirpaths

    filename = dpg.get_value(wfile)
    keyboard.remove_hotkey("enter")
    keyboard.add_hotkey("ctrl+s",writetofile)
    if f"{filename}.txt" in dirpaths:
        infile = open(f"notesapp_data/data/users/userdirectories/{user}/{filename}.txt","rb")
        words = infile.read()
        if words != "":
            words = decrypt(words,user)
        dpg.set_value(wtext,words)
        infile.close()
        dpg.show_item(labelwrite2)
        dpg.show_item(wtext)
        dpg.show_item(savefile)
    else:
        infile = open(f"notesapp_data/data/users/userdirectories/{user}/{filename}.txt","xb")
        dirpaths.append(f"{filename}.txt")
        infile.close()
        dpg.show_item(labelwrite2)
        dpg.show_item(wtext)
        dpg.show_item(savefile)

def writetofile():
    filename = dpg.get_value(wfile)
    words = dpg.get_value(wtext)
    outfile = open(f"notesapp_data/data/users/userdirectories/{user}/{filename}.txt","wb")
    outfile.write(encrypt(words,user))
    outfile.close()
# NOTES APP READ FUNCTIONS

def readnotes():
    global devs
    if user in devs:
        userused = dpg.get_value(ruser)
        filename = dpg.get_value(rfiledev)
        try:
            outfile = open(f"notesapp_data/data/users/userdirectories/{userused}/{filename}.txt","rb")
            notes = decrypt(outfile.read(),userused)
            dpg.set_value(notestext,notes)
            outfile.close()
        except:
            dpg.set_value(notestext,"ERROR: FILE NOT FOUND!")
    else:
        filename = dpg.get_value(rfile)
        try:
            outfile = open(f"notesapp_data/data/users/userdirectories/{user}/{filename}.txt","rb")
            notes = decrypt(outfile.read(),user)
            dpg.set_value(notestext,notes)
            outfile.close()
        except:
            dpg.set_value(notestext,"ERROR: FILE NOT FOUND!")

# DEFINE BUTTON LABELS

writenotesbutton = """                                    
             take notes             
                                    """

readnotesbutton = """                                    
             read notes             
                                    """
helpbutton = """                                    
                help                
                                    """

# NOTES APP MENUS AND WINDOWS

with dpg.window(label="notes", pos=[500,300], width=400, no_background=True, no_title_bar=True, no_move=True, no_resize=True, show=False, tag="notesmenu"):
    # user data and callback set when button is created
    write = dpg.add_button(label=writenotesbutton, callback=showwritenotes)
    read = dpg.add_button(label=readnotesbutton, callback=showreadnotes)
    instructbutton = dpg.add_button(label=helpbutton, callback=instructionsfunc)

with dpg.window(label="showexport", pos=[0,670], width=400, no_background=True, no_title_bar=True, no_move=True, no_resize=True, show=False, tag="showexport"):
    # user data and callback set when button is created
    exportbutton = dpg.add_button(label="export files",callback=showexport)
    importbutton = dpg.add_button(label="import files",callback=showimport)

with dpg.window(label="writemenu", pos=[0,300], width=900,height=500, no_background=True, no_title_bar=True, no_move=True, no_resize=True, show=False, tag="takenotes"):
    # user data and callback set when button is created
    labelwrite = dpg.add_text("name of file you want to write to (if it already exists, will add to end, if doesnt exist, creates it):")
    wfile = dpg.add_input_text(default_value="")
    labelwrite2 = dpg.add_text("write notes here:",show=False)
    wtext = dpg.add_input_text(default_value=f"",height=100,multiline=True,show=False)
    savefile = dpg.add_button(label="save",show=False,callback=writetofile)

with dpg.window(label="readmenu", pos=[0,400], width=900,height=900, no_background=True, no_title_bar=True, no_move=True, no_resize=True, show=False, tag="readnotes"):
    # user data and callback set when button is created
    filelist = dpg.add_text(default_value=f"existing files: {dirpaths} (do not add the .txt at the end of the filename. it adds it automatically and will not work if you do.)")
    labelnotes = dpg.add_text("name of file you want to read:")
    rfile = dpg.add_input_text(default_value="")
    readtext = dpg.add_button(label="click to read!",callback=readnotes)

with dpg.window(label="output", pos=[0,0], width=1000,height=300, no_background=True, no_title_bar=True, no_move=True, no_resize=True, show=False, tag="displaynotes"):
    notestext = dpg.add_text(default_value="",color=(0,255,0),wrap=0)

# GET PATHS FUNCTIONS

def getpaths(hackeduser):
    try:
        hackedpath = []
        parpath = f"{os.getcwd()}/notesapp_data/data/users/userdirectories"
        
        directory = hackeduser
        path = os.path.join(parpath, directory)
        dirallhacked = os.listdir(path)
        for file in dirallhacked:
            
            hackedpath.append(file)
        return hackedpath
    except:
        return ["user does not exist or has never logged in!"]

# BASIC USER FEATURES

def accessanddisplayfilesnondevedition():
    global userpaths
    accesseduser = user
    userpaths = getpaths(accesseduser)
    dpg.set_value(filelistdev,f"existing files: {userpaths} (do not add the .txt at the end of the filename. it adds it automatically and will not work if you do.)")

# DEV FEATURES

def accessanddisplayfiles():
    global dirhackedpaths
    accesseduser = dpg.get_value(ruser)
    dirhackedpaths = getpaths(accesseduser)
    if "user does not exist or has never logged in!" in dirhackedpaths:
        dpg.set_value(notestext,"ERROR: USER DOES NOT EXIST OR THEIR DIRECTORY WAS DELETED/NEVER LOGGED INTO")
    else:
        dpg.show_item(filelistdev)
        dpg.show_item(labelnotesdev)
        dpg.show_item(rfiledev)
        dpg.show_item(readtextdev)
        dpg.set_value(filelistdev,f"existing files: {dirhackedpaths} (do not add the .txt at the end of the filename. \nit adds it automatically and will not work if you do.)")
        keyboard.remove_hotkey("enter")
        keyboard.add_hotkey("enter",readnotes)

with dpg.window(label="readmenu", pos=[0,400], width=900,height=900, no_background=True, no_title_bar=True, no_move=True, no_resize=True, show=False, tag="readnotesdev"):
    # user data and callback set when button is created
    labeluser = dpg.add_text(f"WHICH USER WOULD YOU LIKE TO ACCESS THE FILES OF?")
    ruser = dpg.add_input_text(default_value="")
    raccessuserfiles = dpg.add_button(label="access", callback=accessanddisplayfiles)
    filelistdev = dpg.add_text(default_value="",show=False)
    labelnotesdev = dpg.add_text("name of file you want to read:",show=False)
    rfiledev = dpg.add_input_text(default_value="",show=False)
    readtextdev = dpg.add_button(label="click to read!",callback=readnotes,show=False)

# SYSTEM FUNCTIONS

with dpg.window(label="system", pos=[1200,0], width=200, no_background=True, no_title_bar=True, no_move=True, no_resize=True, show=False, tag="syswin"):
    # user data and callback set when button is created
    dpg.add_button(label="X", callback=exitfunc,width=50)

with dpg.window(label=" ", pos=[1132,680], width=150,height=40, no_title_bar=True, no_move=True, no_resize=True, show=False, tag="timewin"):
    displaytime = dpg.add_text(" ", tag="time")

with dpg.window(label="help", pos=[140,100], width=1000,height=500,no_move=True,no_resize=True, menubar=False, show=False, tag="instructions", no_collapse=True):
    # user data and callback set when button is created
    instructions = dpg.add_text(default_value="")
    close_button = dpg.add_button(label="OK",pos=[490,460],callback=close_help)
    pressoktoclose = dpg.add_text(default_value="press 'ok' to close or press the 'x' button at the top right of this window", before=instructions)

with dpg.window(label="ip", pos=[0,0], width=1000,height=500,no_move=True,no_resize=True, menubar=False, show=False, tag="IP", no_collapse=True):
    USERSIP = dpg.add_text(default_value=f"your ip: {socket.gethostbyname(socket.gethostname())}")

# TIME FUCTION

def starttimefunc():
    global timethread
    global event
    timethread = threading.Thread(target=gettime)
    timethread.daemon = True
    timethread.start()

# THEMES

with dpg.theme() as global_theme:

    with dpg.theme_component(dpg.mvAll):
        dpg.add_theme_color(dpg.mvThemeCol_WindowBg, (0,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 2, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameBorderSize, 1, category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Border, (0, 200, 200), category=dpg.mvThemeCat_Core)

    with dpg.theme_component(dpg.mvButton):
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255, 200, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Button, (0, 0, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Border, (0, 200, 200), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ButtonHovered, (155, 100, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ButtonActive, (255, 200, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 2, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameBorderSize, 1, category=dpg.mvThemeCat_Core)

    with dpg.theme_component(dpg.mvInputText):
        dpg.add_theme_color(dpg.mvThemeCol_FrameBg, (0, 0, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Border, (0, 200, 200), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Text, (0, 255, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 0, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameBorderSize, 1, category=dpg.mvThemeCat_Core)

with dpg.theme() as help_theme:
    with dpg.theme_component(dpg.mvAll):
        dpg.add_theme_color(dpg.mvThemeCol_WindowBg, (0,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Border, (0,200,200), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_TitleBg, (0,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_TitleBgActive, (0,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 0, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameBorderSize, 1, category=dpg.mvThemeCat_Core)

    with dpg.theme_component(dpg.mvText):
        dpg.add_theme_color(dpg.mvThemeCol_Text, (0,255,0))

    with dpg.theme_component(dpg.mvButton):
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 0, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameBorderSize, 1, category=dpg.mvThemeCat_Core)

with dpg.theme() as time_theme:
    with dpg.theme_component(dpg.mvAll):
        dpg.add_theme_color(dpg.mvThemeCol_WindowBg, (0,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Border, (0,200,200), category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 0, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameBorderSize, 1, category=dpg.mvThemeCat_Core)

    with dpg.theme_component(dpg.mvText):
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255,200,0), category=dpg.mvThemeCat_Core)

with dpg.theme() as exit_theme:
    with dpg.theme_component(dpg.mvButton):
        dpg.add_theme_color(dpg.mvThemeCol_Border, (255,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Button, (0,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ButtonHovered, (255,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ButtonActive, (180,0,0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255,255,255), category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 0, category=dpg.mvThemeCat_Core)

with dpg.theme() as login_theme:
    with dpg.theme_component(dpg.mvButton):
        dpg.add_theme_color(dpg.mvThemeCol_Text, (255, 255, 255), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Button, (0, 0, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_Border, (0, 200, 200), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ButtonHovered, (155, 100, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_color(dpg.mvThemeCol_ButtonActive, (255, 200, 0), category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 2, category=dpg.mvThemeCat_Core)
        dpg.add_theme_style(dpg.mvStyleVar_FrameBorderSize, 1, category=dpg.mvThemeCat_Core)

dpg.bind_theme(global_theme)
dpg.bind_item_theme("instructions", help_theme)
dpg.bind_item_theme("timewin", time_theme)
dpg.bind_item_theme("syswin", exit_theme)
dpg.bind_item_theme("loginwindow", login_theme)

# STARTUP STUFF

def fullscreen():
    dpg.toggle_viewport_fullscreen()

keyboard.add_hotkey("f11", fullscreen)
keyboard.add_hotkey("esc + x", exitfunc)
keyboard.add_hotkey("enter",login)
dpg.create_viewport(title='NOTE TAKER', width=1920, height=1080)
dpg.setup_dearpygui()
if getattr(sys, "frozen", False):
    pyi_splash.close()
dpg.show_viewport()

dpg.toggle_viewport_fullscreen()

dpg.start_dearpygui()
dpg.destroy_context()